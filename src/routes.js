import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [{
        path: '/',
        name: 'Главная',
        component: () =>
            import ('./components/Main.vue')
    },
    {
        path: '/create',
        name: 'Создать страницу',
        component: () =>
            import ('./components/Create.vue')
    },
    {
        path: '*',
        name: '404',
        component: () =>
            import ('./components/E404')
    }
]

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

export default router;