import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        pages: []
    },

    mutations: {
        addStartRoutes(state, routes) {
            state.pages = routes
        },

        addPage(state, page) {
            state.pages.push(page)
        }
    },

    actions: {

    }
});